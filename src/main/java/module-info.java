module com.example.javaapplication {
    requires javafx.controls;
    requires javafx.fxml;


    opens com.example.javaapplication to javafx.fxml;
    exports com.example.javaapplication;
}